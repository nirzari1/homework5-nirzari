#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	//methods
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	void draw(const Canvas& canvas);
	void clearDraw(const Canvas& canvas);
	virtual void move(const Point& other);
	virtual double getPerimeter() const;
	virtual double getArea() const;

	
	// override functions if need (virtual + pure virtual)
};