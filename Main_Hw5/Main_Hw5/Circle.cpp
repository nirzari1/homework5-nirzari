#include "Circle.h"

// constructor for circle, and for shape
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type), _center(center.getX(), center.getY())
{
	this->_radius = radius;
}
// destructor for circle, nothing in it - no memory to release
Circle::~Circle()
{

}
// getter for circle center
const Point& Circle::getCenter() const
{
	return _center;
}
// getter for circle radius
double Circle::getRadius() const
{
	return _radius;
}
// draws the circle on the program
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}
// removes the circle from the program
void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}
// getter for circle perimeter
double Circle::getPerimeter() const
{
	double perimeter = _radius * 2 * PI;
	return perimeter;
}
// getter for circle area
double Circle::getArea() const
{
	double area = _radius * _radius * PI;
	return area;
}
// moves the circle by users choice of X and Y
void Circle::move(const Point& other)
{
	_center += other;
}


