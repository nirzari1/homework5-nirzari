#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Circle.h"
#include "Triangle.h"
#include "Arrow.h"
#include "Rectangle.h"
#include <vector>



class Menu
{
public:
	//methods
	Menu();
	~Menu();
	int printFirstMenu();
	void chooseFunction(int choice);
	void addNewShape();
	void addCircle();
	void addArrow();
	void addTriangle();
	void addRectangle();
	void modifyOrGetInformation();
	void moveShape(int shapeNum);
	void deleteAllShapes();
	void removeShape(int shapeNum);

	// more functions..

private:
	//fields
	std::vector<Shape*> _allShapes;
	int shapeCounter = 0;
	Canvas _canvas;
};

