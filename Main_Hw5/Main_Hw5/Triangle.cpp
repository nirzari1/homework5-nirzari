#include "Triangle.h"

// constructor for triangle, and poylgon
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}
// destructor for triangle, nothing to destruct.
Triangle::~Triangle()
{

}
// draws the triangle
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}
// removes the triangle
void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}
// moves the triangle by the given X and Y by the user
void Triangle::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other;
	_points[2] += other; 
}
// getter for triangle perimeter, uses fnc distance from class point to calculate perimeter
double Triangle::getPerimeter() const
{
	double first = 0;
	double second = 0;
	double third = 0;
	first = _points[0].distance(_points[1]); // distance between point1 - point2
	second = _points[0].distance(_points[2]); // ditsance between point1-point3
	third = _points[1].distance(_points[2]); // distance between point2-point3
	double perimeter = first + second + third; // adds all together
	return perimeter;
}
// getter for triangle area
double Triangle::getArea() const
{
	double first = 0;
	double second = 0;
	double third = 0;
	first = _points[0].distance(_points[1]); // distance between point1 - point2
	second = _points[0].distance(_points[2]); // ditsance between point1-point3
	third = _points[1].distance(_points[2]); // distance between point2-point3
	double perimeter = first + second + third; // adds all together
	perimeter = perimeter / 2;
	double area = sqrt(perimeter * (perimeter - first) * (perimeter - second) * (perimeter - third)); // formula to calculate area of triangle given only 3 sides
	return area;
}
