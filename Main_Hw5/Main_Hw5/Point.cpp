#include "Point.h"
#include <math.h>

// Constructor for point
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}
// Copy constructor for point
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}
Point::~Point()
{
	// nothing to destruct/free memory..
}
// getter for point X
double Point::getX() const
{
	return this->_x;
}
// getter for point Y
double Point::getY() const
{
	return this->_y;
}
// operator + to add X and Y of entered point with the current one and return a new point
Point Point::operator+(const Point& other) const
{
	Point to_return(this->_x + other._x, this->_y + other._y);
	return to_return;
}
// operator += to add current x and current y together and save it in the current point
Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}
// function for getting distance between two points (using formula of distance in mathematics)
double Point::distance(const Point& other) const
{
	double dis = 0;
	dis = pow(this->_x - other._x, 2) + pow(this->_y - other._y, 2);
	dis = sqrt(dis);
	return dis;
}