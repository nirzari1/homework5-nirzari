#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		//methods
		virtual double getPerimeter() const;
		virtual double getArea() const;
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);
		virtual ~Rectangle();
		virtual void move(const Point& other);
		virtual void draw(const Canvas& canvas);
		virtual void clearDraw(const Canvas& canvas);

		// override functions if need (virtual + pure virtual)
	protected:
		//fields
		double _width;
		double _length;

	};
}