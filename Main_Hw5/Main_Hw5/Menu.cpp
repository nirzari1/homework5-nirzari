#include "Menu.h"
// constructor for menu (nothing to construct)
Menu::Menu() 
{
}
// destructor for menu (if there are existing shapes, deletes them and releases their memory)
Menu::~Menu()
{
	for (int i = shapeCounter - 1; i >= 0; i--)
	{
		_allShapes[i]->clearDraw(_canvas);
		delete _allShapes[i];
		_allShapes[i] = 0;
		_allShapes.pop_back();
	}
	shapeCounter = 0;
}
// this function prints the first menu and guides to the other functions
int Menu::printFirstMenu()
{
	int retVal = 0;
	system("cls"); // clear screen
	std::cout << "Enter 0 to add a new shape.\n" << "Enter 1 to modify or get information from a current shape.\n" << "Enter 2 to delete all of the shapes.\n" << "Enter 3 to exit.\n";
	std::cin >> retVal;
	while (retVal > 3 || retVal < 0)
	{
		std::cout << "Please enter value between 0 - 3.\n";
		std::cin >> retVal;
		// gets value, while value isnt in the option zone (0-3), it keeps asking for it
	}
	chooseFunction(retVal); // Chooses which function depending on the value
	return retVal;
}
// this function uses to choose what function the user chose in the main menu
void Menu::chooseFunction(int choice)
{
	switch (choice)
	{
		case 0:
			addNewShape(); // chose 0 - add new shape
			break;
		case 1:
			modifyOrGetInformation(); // chose 1 - get info or modify a shape
			break;
		case 2:
			deleteAllShapes(); // chose 2 - deletes everything
			break;
		case 3:
			std::cout << "Good bye!\n"; // chose 3 - leaves program
			break;
	}
}
// this function deletes all the shapes, also releases their memory (if they exist)
void Menu::deleteAllShapes()
{
	for (int i = shapeCounter - 1; i >= 0; i--)
	{
		_allShapes[i]->clearDraw(_canvas); // removes from the program (the draw)
		delete _allShapes[i]; // releases memory
		_allShapes[i] = 0;
		_allShapes.pop_back(); // removes from the vector array
	}
	shapeCounter = 0; // resets shape counter (deleting everything)
}
// this function is used to modify a shape or get information about it or delete it
void Menu::modifyOrGetInformation() // choice = 1
{
	system("cls"); // clear screen
	if (shapeCounter > 0) // if there are any shapes
	{
		for (int i = 0; i < shapeCounter; i++)
		{
			std::cout << "Enter " << i << " for " << _allShapes[i]->getName() << "(" << _allShapes[i]->getType() << ")\n";
		} // prints out all of the shapes options
		int whichShape = 0;
		std::cin >> whichShape;
		while (whichShape < 0 || whichShape > shapeCounter - 1)
		{
			std::cout << "Enter numbers between 0 - " << shapeCounter - 1 << "\n";
			std::cin >> whichShape;
			// gets the users choice on which shape to modify or get information about
		}
		std::cout << "Enter 0 to move the shape.\n";
		std::cout << "Enter 1 to get its details.\n";
		std::cout << "Enter 2 to remove the shape.\n";
		// prints a menu to the user on options of what to do with the shape
		int userPick = 0;
		std::cin >> userPick;
		while (userPick < 0 || userPick > 2)
		{
			std::cout << "Enter numbers between 0 - 2.\n";
			std::cin >> userPick; // while user didnt pick a num between 0-2
		}
		switch (userPick)
		{
			case 0:
				moveShape(whichShape); // user chose 0 (move a shape)
				break;
			case 1:
				_allShapes[whichShape]->printDetails(); // user chose 1 (print the shape details)
				std::cout << "\n";
				system("pause");
				break;
			case 2:
				removeShape(whichShape); // user chose 2 (delete the shape)
				break;
		}
		
	}
}
// this function removes a single shape
void Menu::removeShape(int shapeNum)
{
	_allShapes[shapeNum]->clearDraw(_canvas); // removes its drawing
	delete _allShapes[shapeNum]; // releases memory
	_allShapes[shapeNum] = 0;
	_allShapes.erase(_allShapes.begin() + shapeNum); // removes it from the shape vector array by using erase method
	shapeCounter--; // -1 for shape counter because we just removed a shape
}
// this function is used to move a shape
void Menu::moveShape(int shapeNum)
{
	system("cls"); // clear screen
	double xScale = 0;
	double yScale = 0;
	std::cout << "Please enter the X moving scale: ";
	std::cin >> xScale;
	std::cout << "Please enter the Y moving scale: ";
	std::cin >> yScale; // gets X and Y scale
	Point moveDrawing(xScale, yScale); // constructs a new point
	_allShapes[shapeNum]->clearDraw(_canvas); // removes the current shape drawing
	_allShapes[shapeNum]->move(moveDrawing); // uses move method to change X and Y of current shape
	for (int i = 0; i < shapeCounter; i++)
	{
		_allShapes[i]->clearDraw(_canvas);
		_allShapes[i]->draw(_canvas); // removes all shapes and redraws them, the shape gets re-drawed with the new point 
	}
}
// this function adds a new shape - circle/arrow/triangle/rectangle
void Menu::addNewShape() // choice = 0
{
	int shapeToAdd = 0;
	system("cls"); // clear screen
	std::cout << "Enter 0 to add a circle.\n" << "Enter 1 to add an arrow.\n" << "Enter 2 to add a triangle.\n" << "Enter 3 to add a rectangle.\n";
	std::cin >> shapeToAdd;
	while (shapeToAdd > 3 || shapeToAdd < 0)
	{
		std::cout << "Please enter value between 0 - 3.\n";
		std::cin >> shapeToAdd;
		// prints out menu and until user doesn't pick from the menu the program keeps asking for input
	}
	switch (shapeToAdd)
	{
		case 0:
			addCircle(); // user chose 0 - add circle
			break;
		case 1:
			addArrow(); // user chose 1 - add arrow
			break;
		case 2:
			addTriangle(); // user chose 2 - add triangle
			break;
		case 3:
			addRectangle(); // user chose 3 - add rectangle
			break;
	}
}
// this function adds a circle and draws it
void Menu::addCircle()
{
	std::string type = "Circle";
	double x = 0;
	double y = 0;
	double radius = 0;
	std::string name = "";
	std::cout << "Please enter X:\n";
	std::cin >> x;
	std::cout << "Please enter Y:\n";
	std::cin >> y;
	std::cout << "Please enter radius:\n";
	std::cin >> radius;
	std::cout << "Please enter the name of the shape:\n";
	std::cin >> name;
	// gets all details about the new circle
	Point a(x, y); // creates the center of the circle
	Shape* newCircle = new Circle(a, radius, type, name); // allocates memory for the circle and constructs it
	_allShapes.push_back(newCircle); // adds the new circle to the all shapes vector
	_allShapes[shapeCounter]->draw(_canvas); // draws the circle
	shapeCounter++; // adds 1 to shape counter
}
// this function adds an arrow and draws it
void Menu::addArrow()
{
	std::string type = "Arrow";
	double x = 0;
	double y = 0;
	std::string name = "";
	std::cout << "Please enter X of point number 1:\n";
	std::cin >> x;
	std::cout << "Please enter Y of point number 1:\n";
	std::cin >> y;
	Point a(x, y);
	std::cout << "Please enter X of point number 2:\n";
	std::cin >> x;
	std::cout << "Please enter Y of point number 2:\n";
	std::cin >> y;
	Point b(x, y);
	std::cout << "Enter the name of the shape:\n";
	std::cin >> name;
	// gets all information about the new arrow
	Shape* newArrow = new Arrow(a, b, type, name); // allocates memory for new arrow and constructs it
	_allShapes.push_back(newArrow); // adds the new arrow to the shapes vector
	_allShapes[shapeCounter]->draw(_canvas); // draws the arrow
	shapeCounter++; // adds 1 to shape counter
}
// this function adds a triangle and draws it
void Menu::addTriangle()
{
	double x = 0;
	double y = 0;
	std::string type = "Triangle";
	std::string name = "";
	std::cout << "Please enter X of point number 1:\n";
	std::cin >> x;
	std::cout << "Please enter Y of point number 1:\n";
	std::cin >> y;
	Point a(x, y);
	std::cout << "Please enter X of point number 2:\n";
	std::cin >> x;
	std::cout << "Please enter Y of point number 2:\n";
	std::cin >> y;
	Point b(x, y);
	std::cout << "Please enter X of point number 3:\n";
	std::cin >> x;
	std::cout << "Please enter Y of point number 3:\n";
	std::cin >> y;
	Point c(x, y);
	std::cout << "Enter the name of the shape:\n";
	std::cin >> name;
	// gets all information about the new triangle
	if ((c.getX() == b.getX() && b.getX() == a.getX()) || (c.getY() == b.getY() && b.getY() == a.getY()))
	{
		std::cout << "Cant have a triangle on a straight line!\n";
		system("pause"); // if all X are equal or all Y are equal, the triangle isnt a triangle but a straight line
	}
	else
	{
		Shape* newTriangle = new Triangle(a, b, c, type, name); // allocates memory for the new triangle and constructs it
		_allShapes.push_back(newTriangle); // adds the new triangle to the shapes vector
		_allShapes[shapeCounter]->draw(_canvas); // draws the new triangle
		shapeCounter++; // adds 1 to shape counter
	}
}
// this function adds a rectangle and draws it
void Menu::addRectangle()
{
	std::string type = "Rectangle";
	double x = 0;
	double y = 0;
	double length = 0;
	double width = 0;
	std::string name = "";
	std::cout << "Please enter X of left corner:\n";
	std::cin >> x;
	std::cout << "Please enter Y of left corner:\n";
	std::cin >> y;
	Point a(x, y);
	std::cout << "Please enter the length of the shape:\n";
	std::cin >> length;
	while (length <= 0)
	{
		std::cout << "Please enter a valid length:\n";
		std::cin >> length;
		// length cant be below or equal to 0, keeps asking length until the input is valid
	}
	std::cout << "Please enter the width of the shape:\n";
	std::cin >> width;
	while (width <= 0)
	{
		std::cout << "Please enter a valid width:\n";
		std::cin >> width; // width cant be below or equal to 0, keeps asking width until the input is valid
	}
	std::cout << "Enter the name of the shape:\n";
	std::cin >> name;
	// gets all information about the new rectangle
	Shape* newRectangle = new myShapes::Rectangle(a, length, width, type, name); // allocates memory for the new rectangle and constructs it
	_allShapes.push_back(newRectangle); // adds the new rectangle to the shape vector array
	_allShapes[shapeCounter]->draw(_canvas); // draws the new rectangle 
	shapeCounter++; // adds 1 to shape counter
}

