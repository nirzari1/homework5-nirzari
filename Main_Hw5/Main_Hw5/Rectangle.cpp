#include "Rectangle.h"

// constructor for rectangle and for polygon - creates a new point with given length and width.
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type, name)
{
	_length = length;
	_width = width;
	_points.push_back(a);
	Point secondpoint(_length + a.getX(), _width + a.getY());
	_points.push_back(secondpoint);
}
// destructor for rectangle.. nothing to destruct
myShapes::Rectangle::~Rectangle()
{
	
}
// draws the rectangle
void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}
// removes the rectangle
void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}
// getter for rectangle perimeter
double myShapes::Rectangle::getPerimeter() const
{
	double perimeter = _width * 2 + _length * 2;
	return perimeter;
}
// getter for rectangle area
double myShapes::Rectangle::getArea() const
{
	double area = _width * _length;
	return area;
}
// moves the rectangle with the given X and Y the user entered
void myShapes::Rectangle::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other; // add draw
}


