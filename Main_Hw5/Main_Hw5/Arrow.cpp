#include "Arrow.h"

// Constructor for arrow, shape being constructed too
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(name, type)
{
	_points.push_back(a);
	_points.push_back(b);
}
// no memory to release
Arrow::~Arrow()
{
}
// gets area of arrow
double Arrow::getArea() const
{
	return 0; // area of arrow == 0
}
// gets perimeter of arrow
double Arrow::getPerimeter() const
{
	return (_points[0].distance(_points[1]));
}
// draws the arrow on the program
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
// clears the arrow that was drawn on the program
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}
// moves the arrow on the program by the users choice (X and Y)
void Arrow::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other; 
}


