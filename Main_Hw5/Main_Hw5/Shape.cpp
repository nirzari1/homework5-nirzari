#include "Shape.h"
// constructor for shape
Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}
Shape::~Shape()
{

}
// getterr for shape name
std::string Shape::getName() const
{
	return this->_name;
}
// getter for shape type
std::string Shape::getType() const
{
	return this->_type;
}
// prints all the shape details
void Shape::printDetails() const
{
	std::cout << "Name: " << _name << "| Type: " << _type << "| Area: " << getArea() << "| Perimeter: " << getPerimeter();
}